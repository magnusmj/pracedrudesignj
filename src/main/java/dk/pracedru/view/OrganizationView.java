package dk.pracedru.view;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;

public class OrganizationView extends JPanel {
    MainWindow mainWindow;
    JTree tree;
    public final String ID = "ORGPANEL";
    final JPanel centerPanel;
    JButton createOrgButton;
    JButton removeOrgButton;

    public OrganizationView(MainWindow mainWindow){
        this.mainWindow = mainWindow;
        setLayout(new BorderLayout());
        centerPanel = new JPanel();
        centerPanel.setLayout(new GridBagLayout());
        add(centerPanel);
        tree = initTree();

        initToolbar();
        initDataView();
    }

    private void initDataView() {
        JPanel view = new JPanel();
        view.add(new JLabel("Bente"));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx= 1.0;
        gbc.weighty= 1.0;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.fill =  GridBagConstraints.BOTH;
        centerPanel.add(view,gbc);

    }

    JTree initTree(){
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("root");
        DefaultMutableTreeNode vegetableNode = new DefaultMutableTreeNode("Organization 1");
        DefaultMutableTreeNode fruitNode = new DefaultMutableTreeNode("Organization 2");
        root.add(vegetableNode);
        root.add(fruitNode);

        tree = new JTree(root);
        tree.setRootVisible(false);
        //tree.setMaximumSize(new Dimension(250, 4000));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx= 0.2;
        gbc.weighty= 1.0;
        gbc.insets = new Insets(5, 5, 5, 5);
        //gbc.anchor = GridBagConstraints.PAGE_START;
        gbc.fill =  GridBagConstraints.BOTH;
        centerPanel.add(tree, gbc);
        return tree;
    }
    void initToolbar(){
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        add(toolBar, BorderLayout.NORTH);
        JLabel label = new JLabel("Organization View  ");
        Font f = label.getFont();
        label.setFont(f.deriveFont(f.getStyle() | Font.BOLD));

        toolBar.add(label);
        createOrgButton = new JButton("Create Organization");
        createOrgButton.addActionListener(e -> onCreateOrg());
        toolBar.add(createOrgButton);
        removeOrgButton = new JButton("Remove Organization");
        removeOrgButton.setEnabled(false);
        createOrgButton.addActionListener(e -> onTemoveOrg());
        toolBar.add(removeOrgButton);
    }

    private void onTemoveOrg() {

    }

    void onCreateOrg(){

    }
}
