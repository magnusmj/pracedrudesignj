package dk.pracedru.view;

import dk.pracedru.controller.InteropWebSocketServer;
import org.apache.log4j.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;


public class MainWindow extends JFrame {
    static Logger logger = Logger.getLogger(MainWindow.class);
    private ViewBar viewBar;
    private final Thread serverInteropThread;
    private final Thread clientInteropThread;
    JButton orgViewBtn;
    JButton projectViewBtn;
    JButton sketchViewBtn;
    JButton partViewBtn;
    JButton assemblyViewBtn;
    CardLayout cardLayout;
    JPanel cardsPanel;
    OrganizationView organizationView;
    ProjectView projectView;
    SketchView sketchView;
    PartView partView;
    InteropWebSocketClient interopWebSocketClient;
    public MainWindow(Thread serverInteropThread) {
        this.serverInteropThread = serverInteropThread;
        try {
            String uiLFName = UIManager.getSystemLookAndFeelClassName();
            UIManager.setLookAndFeel(uiLFName);
        } catch (Exception e) {
            logger.info("Theme not found");
        }
        handleWindowEvents(this);
        setSize(1600, 900); // Example size
        setLocationRelativeTo(null); // Center on screen
        setVisible(true);
        initializeContent();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Pracedru Design");
        clientInteropThread = new Thread(InteropWebSocketClient::startInterop);
        clientInteropThread.start();
    }

    private void handleWindowEvents(MainWindow w){
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                logger.debug("closing interop");// todo: implement close dialog
                serverInteropThread.interrupt();
            }
            @Override
            public void windowClosed(WindowEvent e) {
                serverInteropThread.interrupt();
                try {
                    clientInteropThread.join();
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
    }
    private Icon loadIcon(String resourcePath){
        try {
            URL url =  this.getClass().getResource(resourcePath);
            BufferedImage image = ImageIO.read(url);
            return new ImageIcon(image.getScaledInstance(32, 32,  java.awt.Image.SCALE_SMOOTH));
        } catch (IOException | IllegalArgumentException e) {
            logger.error("icon not found: " + resourcePath);
        }
        return null;
    }
    private void initializeContent(){
        viewBar = new ViewBar();
        add(viewBar, BorderLayout.WEST);
        orgViewBtn = viewBar.createButton(this::setOrganizationView,  loadIcon("/icons/org.png"), "Organization View");
        projectViewBtn = viewBar.createButton(this::setProjectView, loadIcon("/icons/project.png"), "Project view");
        sketchViewBtn = viewBar.createButton(this::setSketchView, loadIcon("/icons/sketch.png"), "Sketch view");
        partViewBtn = viewBar.createButton(this::setPartView, loadIcon("/icons/part.png"), "Part view");

        cardLayout = new CardLayout();
        cardsPanel = new JPanel(cardLayout);
        add(cardsPanel);
        organizationView = new OrganizationView(this);
        projectView = new ProjectView(this);
        sketchView = new SketchView(this);
        partView = new PartView(this);
        cardsPanel.add(organizationView, organizationView.ID);
        cardsPanel.add(projectView, projectView.ID);
        cardsPanel.add(sketchView, sketchView.ID);
        cardsPanel.add(partView, partView.ID);
    }
    private void setPartView() {
        cardLayout.show(cardsPanel, partView.ID);
    }
    private void setSketchView() {
        cardLayout.show(cardsPanel, sketchView.ID);
    }
    private void setOrganizationView(){
        cardLayout.show(cardsPanel, organizationView.ID);
    }
    private void setProjectView(){
        cardLayout.show(cardsPanel, projectView.ID);
    }
}