package dk.pracedru.view;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;

public class ProjectView extends JPanel {
    final MainWindow mainWindow;
    final JTree tree;
    public final String ID = "PRJPANEL";

    public ProjectView(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        setLayout(new BorderLayout());
        tree = initTree();
        initToolbar();
    }
    JTree initTree(){
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Project 1");
        DefaultMutableTreeNode vegetableNode = new DefaultMutableTreeNode("Sketch 1");
        DefaultMutableTreeNode fruitNode = new DefaultMutableTreeNode("Part 1");
        root.add(vegetableNode);
        root.add(fruitNode);
        JTree tree = new JTree(root);
        add(tree);
        return tree;
    }
    void initToolbar(){
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        add(toolBar, BorderLayout.NORTH);
        JLabel label = new JLabel("Project View");
        Font f = label.getFont();
        label.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
        toolBar.add(label);
    }
}
