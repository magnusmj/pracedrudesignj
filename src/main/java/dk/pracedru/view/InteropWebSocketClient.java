package dk.pracedru.view;

import dk.pracedru.controller.InteropMessageHandler;
import org.apache.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.eclipse.jetty.websocket.common.WebSocketSession;
import org.eclipse.jetty.websocket.common.WebSocketSessionListener;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.*;


public class InteropWebSocketClient extends WebSocketClient {
    static Logger logger = Logger.getLogger(InteropWebSocketClient.class);
    public static final InteropWebSocketClient instance = new InteropWebSocketClient();
    public final BlockingQueue queue = new LinkedBlockingQueue();

    private InteropWebSocketClient(){
        try {
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void startInterop()  {
        EventEndpoint socket = new EventEndpoint();

        URI destUri = null;
        try {
            destUri = new URI("ws://localhost:8080/data");
        } catch (URISyntaxException e) {
            logger.error(e);
        }

        ClientUpgradeRequest request = new ClientUpgradeRequest();

        try {
            instance.connect(socket, destUri, request);
        } catch (Exception e) {
            logger.error(e);
        }
        try {
            socket.awaitClose();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    static class EventEndpoint extends WebSocketAdapter {
        static Logger logger = Logger.getLogger(EventEndpoint.class);
        private final CountDownLatch closureLatch = new CountDownLatch(1);

        @Override
        public void onWebSocketConnect(Session sess)
        {
            super.onWebSocketConnect(sess);
            try {
                sess.getRemote().sendString("{type:1}");
            } catch (IOException e) {
                logger.error(e);
            }
        }

        @Override
        public void onWebSocketText(String message)
        {
            super.onWebSocketText(message);
            logger.debug("Received TEXT message: " + message);
        }

        @Override
        public void onWebSocketClose(int statusCode, String reason)
        {
            super.onWebSocketClose(statusCode, reason);
            closureLatch.countDown();
        }

        @Override
        public void onWebSocketError(Throwable cause)
        {
            super.onWebSocketError(cause);
            cause.printStackTrace(System.err);
        }

        public void awaitClose() throws InterruptedException
        {
            closureLatch.await();
            try {
                instance.stop();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
