package dk.pracedru.view;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;

public class PartView extends JPanel {
    public final String ID = "PRTPANEL";
    final MainWindow mainWindow;
    JTree tree;
    public PartView(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        setLayout(new BorderLayout());
        initTree();
        initToolbar();
    }
    void initTree(){
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Part 1");
        DefaultMutableTreeNode vegetableNode = new DefaultMutableTreeNode("Sketch");
        DefaultMutableTreeNode fruitNode = new DefaultMutableTreeNode("Revolve");
        DefaultMutableTreeNode node = new DefaultMutableTreeNode("Extrude");
        root.add(vegetableNode);
        root.add(fruitNode);
        root.add(node);

        tree = new JTree(root);
        add(tree);
    }
    void initToolbar(){
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        add(toolBar, BorderLayout.NORTH);
        JLabel label = new JLabel("Part View");
        Font f = label.getFont();
        label.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
        toolBar.add(label);
    }
}
