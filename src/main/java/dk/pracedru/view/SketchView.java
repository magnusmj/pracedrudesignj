package dk.pracedru.view;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;

public class SketchView extends JPanel {
    public final String ID = "SKTPANEL";
    final MainWindow mainWindow;
    JTree tree;
    public SketchView(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        setLayout(new BorderLayout());
        initTree();
        initToolbar();
    }
    void initTree(){
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Sketch 1");
        DefaultMutableTreeNode vegetableNode = new DefaultMutableTreeNode("Points");
        DefaultMutableTreeNode fruitNode = new DefaultMutableTreeNode("Lines");
        DefaultMutableTreeNode node = new DefaultMutableTreeNode("Areas");
        root.add(vegetableNode);
        root.add(fruitNode);
        root.add(node);

        tree = new JTree(root);
        add(tree);
    }
    void initToolbar(){
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        add(toolBar, BorderLayout.NORTH);
        JLabel label = new JLabel("Sketch View");
        Font f = label.getFont();
        label.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
        toolBar.add(label);
    }
}
