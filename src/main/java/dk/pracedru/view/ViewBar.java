package dk.pracedru.view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class ViewBar extends JToolBar {
    public ViewBar(){
        setFloatable(false);
        setOrientation(VERTICAL);
    }
    public JButton createButton(Runnable action, Icon icon, String toolTipText){
        JButton button = new JButton();
        button.setMargin(new Insets(0, 0, 0, 0));
        button.setIcon(icon);
        button.addActionListener(e -> action.run());
        button.setToolTipText(toolTipText);
        add(button);
        return button;
    }
}
