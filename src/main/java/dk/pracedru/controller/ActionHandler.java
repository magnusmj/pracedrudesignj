package dk.pracedru.controller;

import org.apache.log4j.Logger;

public class ActionHandler {
    static Logger logger = Logger.getLogger(ActionHandler.class);
    public final static ActionHandler instance = new ActionHandler();
    private ActionHandler(){}

    public void handleAction(Action action) {
        logger.debug(action.type);

    }
}
