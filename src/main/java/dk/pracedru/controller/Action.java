package dk.pracedru.controller;

import com.google.gson.annotations.SerializedName;

public class Action {
    ActionType type;
    long subjectID;
    public Action(ActionType type, long subjectID){
        this.subjectID = subjectID;
    }
}
