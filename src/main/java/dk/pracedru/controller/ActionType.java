package dk.pracedru.controller;

import com.google.gson.annotations.SerializedName;

public enum ActionType {
    @SerializedName("0")
    CreateOrganization,
    @SerializedName("1")
    CreateProject,
    @SerializedName("2")
    CreateParameter,
    @SerializedName("3")
    CreateVertex,
    @SerializedName("4")
    CreateSketch,
    @SerializedName("5")
    CreatePart,
    @SerializedName("6")
    CreateAssembly,
    @SerializedName("7")
    CreateDrawing,
    @SerializedName("8")
    CreateCalcSheet,
    @SerializedName("9")
    ChangeNumberValue,
    @SerializedName("10")
    ChangeName,
    @SerializedName("11")
    Undo,
    @SerializedName("12")
    Redo,
    @SerializedName("13")
    Connect
}
