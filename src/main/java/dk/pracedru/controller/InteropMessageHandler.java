package dk.pracedru.controller;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.log4j.Logger;
import java.util.Arrays;

public class InteropMessageHandler {
    static Logger logger = Logger.getLogger(InteropMessageHandler.class);
    public static final InteropMessageHandler instance = new InteropMessageHandler();
    Gson gson = new Gson();
    private InteropMessageHandler(){}
    public void handleMessage(String msg){
        try {
            Action action = gson.fromJson(msg, Action.class);
            ActionHandler.instance.handleAction(action);
        } catch (JsonSyntaxException e){
            logger.error("Message not an Action", e);
        } catch (Exception e){
            logger.error("Message Exception", e);
        }
    }
}
