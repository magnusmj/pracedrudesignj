package dk.pracedru.controller;

import org.apache.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

@WebSocket(maxIdleTime=3000000)
public class InteropWebSocketServer {
    private static final CountDownLatch stopLatch = new CountDownLatch(1);
    static final List<InteropWebSocketServer> interopWebSockets = new ArrayList<>();
    static Logger logger = Logger.getLogger(InteropWebSocketServer.class);
    static int counter = 0;
    Session session;

    @OnWebSocketConnect
    public void onConnect(Session new_session) throws IOException {
        session = new_session;
        counter++;
        interopWebSockets.add(this);
    }

    @OnWebSocketMessage
    public void onMessage(String msg) throws IOException {

        InteropMessageHandler.instance.handleMessage(msg);
        session.getRemote().sendString(msg);
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason)  {
        interopWebSockets.remove(this);
    }

    @OnWebSocketError
    public void onError(Throwable throwable) {
        logger.info(throwable);
    }

    public void sendMessage(String msg){
        try {
            session.getRemote().sendString(msg);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void sendMessageToAll(String msg){
        interopWebSockets.forEach((n)-> n.sendMessage(msg));
    }

    public static void startInterop(){
        Server server = new Server(8080);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        WebSocketHandler wsHandler = new WebSocketHandler() {
            @Override
            public void configure(WebSocketServletFactory factory) {
                factory.register(InteropWebSocketServer.class);
            }
        };
        context.setHandler(wsHandler);
        server.setHandler(context);
        try {
            server.start();
        } catch (Exception e) {
            logger.error("exception in websocket", e);
        }

        try {
            stopLatch.await();
        } catch (InterruptedException e) {
            boolean i = Thread.interrupted();
            stopLatch.countDown();
        }
        stopInterop(server);

    }
    public static void stopInterop(Server server){
        try {
            server.setStopTimeout(5);
            server.stop();
        } catch (InterruptedException e) {
            boolean i = Thread.interrupted();
            stopInterop(server);
        } catch (Exception e) {
            logger.error(e);
        }
        server.destroy();
    }
}
