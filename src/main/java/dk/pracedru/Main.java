package dk.pracedru;

import dk.pracedru.controller.InteropWebSocketServer;
import dk.pracedru.view.MainWindow;
import org.apache.log4j.BasicConfigurator;


public class Main {
    public static void main(String[] args) {
        BasicConfigurator.configure(); // simple logger
        Thread interopThread = new Thread(InteropWebSocketServer::startInterop);
        interopThread.start();
        MainWindow mw =  new MainWindow(interopThread);
        mw.setVisible(true);
    }


}