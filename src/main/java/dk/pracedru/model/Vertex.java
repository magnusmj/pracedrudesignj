package dk.pracedru.model;

public class Vertex {
    public double x;
    public double y;
    public double z;
    public Vertex(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public double length(){
        return Math.sqrt(x*x+y*y+z*z);
    }
}

