package dk.pracedru.model;

import dk.pracedru.controller.Action;

import java.util.ArrayList;
import java.util.List;

public class ChangeTreeNode {
    private final List<ChangeTreeNode> childNodes = new ArrayList<>();
    public final Action action;
    public final ChangeTreeRoot root;
    public ChangeTreeNode(ChangeTreeRoot root, Action action){
        this.action = action;
        this.root = root;
    }
    public void makeChange(Action action){
        this.childNodes.add(new ChangeTreeNode(root, action));
    }
}
